#include <ostream>
#include <vector>
#include <functional>
#include <iostream>

using namespace std;
class reactive_property
{
  public:
    friend ostream& operator<<(ostream& out, reactive_property& rp)
    {
        out << rp.m_property;
        return out;
    }

    reactive_property(string name, int value) : name(name), m_property(value){};
    reactive_property(string name, reactive_property* other)
    {
        this->name = name;
        other->add_observer(this);
    };

    int getValue() { return m_property; }

    void setProp(int prop)
    {
        if (!reacted)
        {
            cout << name << " reacted" << std::endl;
            reacted  = true;
            m_property = prop;
            for (auto& connection : connections)
            {
                connection->setProp(prop);
            }
        }
        else
        {
            cout << name << "  not reacted" << std::endl;
        }
        reacted = false;
    }

    void add_observer(reactive_property* other)
    {
        other->m_property = m_property;
        connections.push_back(other);
    }

    void connect(reactive_property* other)
    {
        other->m_property = m_property;
        connections.push_back(other);
        other->add_observer(this);
    }

  private:
    std::string name;
    std::vector<reactive_property*> connections;
    bool reacted = false;
    int m_property;
};

int main()
{

    reactive_property a("a", 20);
    reactive_property o("o", &a);

    cout << "#1 " << a << " " << o << endl;
    a.setProp(30);
    cout << "#2 " << a << " " << o << endl;

    reactive_property c("c", 0);
    a.connect(&c);

    cout << "#3 " << a << " " << o << " " << c << endl;
    a.setProp(40);
    cout << "#4 " << a << " " << o << " " << c << endl;
    c.setProp(10);
    cout << "#5 " << a << " " << o << " " << c << endl;
}