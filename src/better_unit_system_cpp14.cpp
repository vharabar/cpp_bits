#include <cstdint>
#include <functional>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>

namespace Units
{
// clang-format off
#define ADD_UNITS(add) \
        add(Metres) \
        add(Kilometers) \
        add(Miles) \
        add(Pascals) \
        add(Bars) \
        add(Volts) \
        add(Watts) \
        add(HorsePower)

#define ADD_CONVERSIONS(add) \
        add(Kilometers, Metres, 1000.0) \
        add(Bars, Pascals, 10000.0) \
        add(Miles, Kilometers, 1.60934)

// clang-format on

#define COMMA_SEPARATED(x) x,

enum class UnitsEnum
{
    ADD_UNITS(COMMA_SEPARATED)
};

} // namespace Units

/**
 * This verbose template specialization can be replaced with a lambda and
 * decltype in c++20
 */
namespace std
{
template <> struct std::hash<tuple<Units::UnitsEnum, Units::UnitsEnum>>
{
    size_t
    operator()(tuple<Units::UnitsEnum, Units::UnitsEnum> const& key) const
    {
        std::uint32_t h = static_cast<std::uint16_t>(std::get<0>(key)) << 16;
        h &= static_cast<std::int16_t>(std::get<1>(key));
        return h;
    }
};
} // namespace std

namespace Units
{
using std::make_tuple;
using std::tuple;
using std::unordered_map;

auto const tuple_hash =
    [](tuple<Units::UnitsEnum, Units::UnitsEnum> const& key) -> size_t
{
    std::uint32_t h = static_cast<std::uint16_t>(std::get<0>(key)) << 16;
    h &= static_cast<std::int16_t>(std::get<1>(key));
    return h;
};

#define CONVERSION_ENTRY(from, to, val)                                        \
    {make_tuple(UnitsEnum::from, UnitsEnum::to), val},                         \
        {make_tuple(UnitsEnum::to, UnitsEnum::from), 1.0 / val},

// c++20 version
// const static unordered_map<tuple<UnitsEnum, UnitsEnum>, double,
// decltype(tuple_hash)> UnitConversionConstants = {
//     ADD_CONVERSIONS(CONVERSION_ENTRY)};

const static unordered_map<tuple<UnitsEnum, UnitsEnum>, double>
    UNIT_CONVERSION_CONSTANTS = {ADD_CONVERSIONS(CONVERSION_ENTRY)};

struct BasicUnit
{
    const UnitsEnum unitType;
    const double value;
    char const* const name;
    operator double() { return value; }
};

#define DERIVED_TYPES(x)                                                       \
    struct x : public BasicUnit                                                \
    {                                                                          \
        constexpr static UnitsEnum UnitType = UnitsEnum::x;                    \
        x(double value) : BasicUnit({UnitsEnum::x, value, #x}) {}              \
        x(BasicUnit& other)                                                    \
            : x(other.value * UNIT_CONVERSION_CONSTANTS.at(                    \
                                  make_tuple(other.unitType, UnitsEnum::x)))   \
        {                                                                      \
        }                                                                      \
    };

ADD_UNITS(DERIVED_TYPES)

template <class To, class From>
std::enable_if_t<std::is_base_of<BasicUnit, To>::value &&
                     (std::is_base_of<BasicUnit, To>::value ||
                      std::is_same<BasicUnit, From>::value),
                 To>
convertTo(From f)
{
    return To(f);
}

// Macro cleanup
#undef DERIVED_TYPES
#undef ADD_UNITS
#undef COMMA_SEPARATED
#undef ADD_CONVERSIONS
#undef CONVERSION_ENTR
#undef FORWARD_CLASS_DECLARATION
}; // namespace Units

int main()
{
    using namespace std;
    using namespace Units;

    auto a = Metres(2000);

    cout << a.name << " " << a << std::endl;

    Kilometers b = a;

    cout << b.name << " " << b << std::endl;

    Miles c = b;

    cout << c.name << " " << c << std::endl;

    Metres d = Units::convertTo<Metres>(b);
}