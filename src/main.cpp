#include <async_single_fifo.h>
#include <cstdio>
#include <memory>
#include <vector>
#include <worker_pool.h>

#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <string>

using namespace std;

class foo {
  public:
    int i;
    foo(int _i) : i(_i){};
    void operator()(int a) { printf("%d ~ %d\n", i, a); }
};

int main(/*int argc, char *argv[]*/) {
    using namespace std;
    auto start = chrono::high_resolution_clock::now();

    printf("creating worker pool\n");
    fancy::worker_pool wp(20);
    printf("creating jobs\n");

    for (int i = 0; i < 10'000; i++) {
        wp.submit(foo(i), i / 2);
    }

    printf("waiting for jobs to finish\n");
    wp.wait_until_empty();
    printf("finished jobs, starting queue\n");

    shared_ptr queue = make_shared<fancy::async_single_fifo<int>>(5);

    shared_ptr test_data = make_shared<vector<int>>(10000);
    shared_ptr test_result = make_shared<vector<int>>(10000);

    for (int i = 0; i < test_data->size(); i++) {
        test_data->at(i) = i;
    }
    printf("generated data, starting test\n");

    wp.submit(
        [](shared_ptr<vector<int>> _in, shared_ptr<fancy::async_single_fifo<int>> _queue) {
            for (int i = 0; i < _in->size(); i++) {
                _queue->push(_in->at(i));
                // printf("reading %d\n", i);
            }
        },
        test_data, queue);

    wp.submit(
        [](shared_ptr<vector<int>> _out, shared_ptr<fancy::async_single_fifo<int>> _queue) {
            for (int i = 0; i < _out->size(); i++) {
                _out->at(i) = _queue->front();
                _queue->pop();
                // printf("popping %d\n", i);
            }
        },
        test_result, queue);

    printf("waiting for queue to finish\n");
    wp.wait_until_empty();
    printf("queue finished");
    wp.stop();

    printf("verifying result\n");
    for (int i = 0; i < test_result->size(); i++) {
        int r = test_result->at(i);
        printf("element %d is %d \n", i, r);
    }

    printf("program ended\n");
    auto end = chrono::high_resolution_clock::now();
    auto elapsed = end - start;
    cout << "time elapsed: " << chrono::duration_cast<chrono::milliseconds>(elapsed).count() << "ms" << endl;

    // getchar();
    return 0;
}
