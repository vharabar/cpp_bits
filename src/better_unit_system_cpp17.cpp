#include <any>
#include <iostream>
#include <sstream>
#include <string>
#include <variant>
#include <vector>

/**
 * Type-based unit system with partial unit to unit conversion
 */
namespace Units
{

/**
 * Convenience methods for adding units and conversions
 */

// clang-format off
#define CREATE_UNITS_SEP(add, SEP)                                            \
  add(Metres)                                                                 \
  SEP add(Kilometers)                                                         \
  SEP add(Pascals)                                                            \
  SEP add(Bars)                                                               \
  SEP add(Volts)                                                              \
  SEP add(Amps)

#define CREATE_CONVERTIRONS(add)                                              \
  add(Kilometers, Metres, 1000)                                               \
  add(Bars, Pascals, 100000)
// clang-format on

/************************************************************
 *                          !!!!!!!!!                       *
 * Don't edit below here unless you know what you are doing *
 ************************************************************/

// Base unit template is restricted to arithmetic units only
template <class T> class base_unit
{
  public:
    using value_t = T;
    virtual operator T() { return _value; }
    virtual T value() { return _value; }
    std::enable_if_t<std::is_arithmetic_v<T>, double> valueAsDouble()
    {
        return static_cast<double>(_value);
    }
    virtual const char* name() = 0;

  protected:
    base_unit(T _value) : _value(_value) {}
    T _value;
};

// This template is intended to be used only with specialization
// General template allows for swapping position of template arguments
// to reduce number of specializations by 2
template <class From, class To> struct _conversion_constants
{
    constexpr static double value = 1. / _conversion_constants<To, From>::value;
};

// Macros to generate classes for units derived from base_unit
#define CREATE_UNITS(func) CREATE_UNITS_SEP(func, )

#define DEFINE_UNIT(x)                                                         \
    class x : public base_unit<float>                                          \
    {                                                                          \
      public:                                                                  \
        x(float f) : base_unit(f) {}                                           \
        template <class T> T convertTo()                                       \
        {                                                                      \
            return {static_cast<float>(_value *                                \
                                       _conversion_constants<x, T>::value)};   \
        }                                                                      \
        virtual const char* name() { return #x; };                             \
    };

CREATE_UNITS(DEFINE_UNIT)

// Macros to generate a std::variant with every unit class
#define NOOP(x) x
#define COMMA   ,

using Unit = std::variant<CREATE_UNITS_SEP(NOOP, COMMA)>;

// Visitor type definition
template <class... Ts> struct visitor_t : Ts...
{
    using Ts::operator()...;
};
// explicit deduction guide (not needed as of C++20)
template <class... Ts> visitor_t(Ts...) -> visitor_t<Ts...>;

// Macros to generate conversion rates for pairs of units.
// It is sufficient to define it of each pair once, the
// inverse value is automatically handled
#define ADD_CONVERSION(from, to, val)                                          \
    template <> struct _conversion_constants<from, to>                         \
    {                                                                          \
        constexpr static double value = val;                                   \
    };

CREATE_CONVERTIRONS(ADD_CONVERSION)

template <class To, class From> To convertTo(From arg)
{
    return arg.value() * _conversion_constants<From, To>::value;
}

// macro cleanup
#undef DEFINE_UNIT
#undef CREATE_UNITS_SEP
#undef CREATE_UNITS
#undef NOOP
#undef ADD_CONVERSION
} // namespace Units

int main()
{
    using namespace Units;

    auto a = Units::_conversion_constants<Kilometers, Metres>::value;
    auto b = Units::_conversion_constants<Metres, Kilometers>::value;

    std::cout << a << " " << b << std::endl;

    std::vector<Unit> list{Metres(20), Pascals(20), Volts(99), Bars(9),
                           Amps(1)};

    try
    {
        auto n = std::get<Volts>(list.at(0));
        std::cout << n.name();
    }
    catch (std::bad_variant_access e)
    {
        std::cout << "BAD ACCESS " << e.what() << std::endl;
    }
    auto PrettyPrinter =
        visitor_t{[](Volts m)
                  {
                      std::stringstream ss;
                      ss << "Volts " << m.value() << std::endl;
                      return ss.str();
                  },
                  [](Pascals m)
                  {
                      std::stringstream ss;
                      ss << "Pascals " << m.value() << std::endl;
                      return ss.str();
                  },
                  [](Bars b)
                  {
                      std::stringstream ss;
                      ss << "Converting Bars: " << b
                         << " to Pascals: " << b.convertTo<Pascals>()
                         << std::endl;
                      return ss.str();
                  },
                  [](Metres m)
                  {
                      std::stringstream ss;
                      ss << "Metres " << m.value() << std::endl;
                      return ss.str();
                  },
                  [](Kilometers m)
                  {
                      std::stringstream ss;
                      ss << "Kilometers " << m.value() << std::endl;
                      return ss.str();
                  },
                  [](auto a)
                  {
                      std::stringstream ss;
                      ss << "Missing handler for " << a.name() << std::endl;
                      return ss.str();
                  }};

    for (auto& e : list)
    {
        std::cout << std::visit(PrettyPrinter, e);
    }
};
