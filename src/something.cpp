#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std::string_literals;

namespace vharabar
{

template <class T> class trie
{
  protected:
    struct node
    {
        std::string key;
        T value;
        std::unordered_map<std::string, node> children;
    };

    std::unique_ptr<node> root_node;

    size_t common_prefix(std::string_view s1, std::string_view s2)
    {
        if (s1[0] != s2[0])
            return 0;

        size_t len = std::min(s1.size(), s2.size());
        size_t it;

        for (it = 1; it < len; it++)
            if (s1[it] != s2[it])
                break;

        return it;
    }

  public:



  void set(std::string_view key, T value){

  }

};
} // namespace vharabar


    size_t common_prefix(std::string_view s1, std::string_view s2)
    {
        if (s1[0] != s2[0])
            return {};

        size_t len = std::min(s1.size(), s2.size());
        size_t it;

        for (it = 1; it < len; it++)
            if (s1[it] != s2[it])
                break;

        return it;
    }

int main()
{


    std::cout << common_prefix("this is my domain", "this is my den");
    // comment to prevent collapse
}