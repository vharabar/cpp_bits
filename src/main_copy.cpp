#include <algorithm>
#include <atomic>
#include <cstddef>
#include <functional>
#include <iostream>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <vector>

template <class T> class IObservable {
public:
  using subKey = std::size_t;
  virtual ~IObservable() = default;
  [[nodiscard(
      "You must use this value for eventual cleanup")]] std::function<void()>
  subscribe(std::function<void(const T &)> observer) {
    subKey key = nonce++;
    observers[key] = observer;
    return {[this, key]() { this->unsubscribe(key); }};
  }
  void unsubscribe(subKey key) { observers.erase(key); }

protected:
  IObservable() = default;
  // No move no copy because IObserver storesn a reference to this
  IObservable(const IObservable &) = delete;
  IObservable &operator=(const IObservable &) = delete;

  void notify(T &value) {

    std::for_each(observers.cbegin(), observers.cend(),
                  [=](auto f) { f.second(value); });
  }

private:
  std::unordered_map<subKey, std::function<void(const T &)>> observers;
  std::atomic_uint32_t nonce;
};

class IObserver {

public:
  virtual ~IObserver() {

    for (auto &func : subscriptions) {
      func();
    }
  };

protected:
  IObserver() = default;
  IObserver(const IObserver &) = delete;
  IObserver &operator=(const IObserver) = delete;
  IObserver(const IObserver &&other) {
    subscriptions = std::move(other.subscriptions);
  }
  IObserver &operator=(const IObserver &&other) {
    subscriptions = std::move(other.subscriptions);
    return *this;
  }

  template <class T>
  void subscribeTo(IObservable<T> &subject,
                   std::function<void(const T &)> function) {
    subscriptions.push_back(subject.subscribe(function));
  }

private:
  std::vector<std::function<void()>> subscriptions;
};

class Foo : public IObservable<std::string> {
public:
  Foo(std::string ss) : s(ss) {}
  void doTheThing() { notify(s); }

protected:
  std::string s;
};

class Bar : public IObserver {
public:
  Bar(IObservable<std::string> &s) {
    subscribeTo<std::string>(s, std::function<void(const std::string &)>(
                                    [](const std::string &sv) -> void {
                                      std::cout << sv << std::endl;
                                    }));
  }
};

int main() {

  auto func = [](std::string s) { std::cout << s << std::endl; };

  Foo foo{"bar"};

  Bar bar{foo};

  foo.doTheThing();
}