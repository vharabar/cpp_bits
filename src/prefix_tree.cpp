#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>
#include <optional>

using namespace std::string_literals;
template <typename T> class prefix_tree
{
  public:
    void set(std::string_view key, T value)
    {
        std::cout << " < " << key << std::endl;
        auto child = find_candidate(root_node, key);
        if (!child.has_value())
        {
            auto child_key = std::string(key);
            root_node.push_back({child_key, value, {}});
        }
        else
        {
            update_node(child.value().second, key, value);
        }
    }

    void remove(std::string_view key)
    {
        std::cout << " > " << key << std::endl;
        remove_value(root_node, key);
    }

    void visualize_trie()
    {
        std::cout << "[";
        for (auto& child : root_node)
        {
            std::cout << std::endl;
            visualize_node(1, &child);
            std::cout << std::endl;
        }
        std::cout << std::endl << "]" << std::endl;
    }

  protected:
    struct node
    {
        std::string key;
        std::optional<T> value;
        std::vector<node> children;
    };
    using children_t = std::vector<node>;

    // this is a virtual root node that has key length 0 and no value;
    children_t root_node;

    void remove_value(children_t& children, std::string_view key)
    {
        // first find a candidate
        auto candidate = find_candidate(children, key);
        if (!candidate.has_value())
        {
            return;
        }

        auto [node_index, node] = candidate.value();
        auto prefix_len         = common_prefix_len(key, node->key);
        // if node key does not match the key do nothing
        if (node->key.size() > prefix_len)
        {
            return;
        }

        // if we got exact match remove value
        if (node->key.size() == prefix_len && key.size() == prefix_len)
        {
            node->value = {};
        }
        else
        {
            // the only other option is partial match, i.e node->key.size() <
            // prefix_len
            remove_value(node->children, key.substr(prefix_len));
        }
        
        // if node has no children
        if (node->children.size() == 0)
        {
            children.erase(children.begin() + node_index);
            return;
        }

        // if node has a single child then collapse
        if (node->children.size() == 1)
        {
            auto child     = node->children.at(0);
            node->key      = node->key + child.key;
            node->value    = child.value;
            node->children = child.children;
        }
    }

    static size_t common_prefix_len(std::string_view s1, std::string_view s2)
    {
        if (s1[0] != s2[0])
            return 0;

        size_t len = std::min(s1.size(), s2.size());
        size_t it;

        for (it = 1; it < len; it++)
            if (s1[it] != s2[it])
                break;

        return it;
    }

    std::optional<std::pair<int, node*>> find_candidate(children_t& children,
                                                        std::string_view key)
    {
        for (int i = 0; i < children.size(); i++)
        {
            auto& child = children.at(i);
            if (child.key[0] == key[0])
            {
                return {{i, &child}};
            }
        }

        return {};
    }

    void update_node(node* node, std::string_view key, T value)
    {
        // We assume that prefix_len >= 1
        int prefix_len = common_prefix_len(key, node->key);

        // key is exact match with current node
        if (prefix_len == node->key.size() && prefix_len == key.size())
        {
            node->value = value;
            return;
        }

        // key is not exact match

        // root key is longer than common prefix, so we split it into a new
        // child
        if (node->key.size() > prefix_len)
        {
            auto old_root_key       = node->key;
            node->key               = old_root_key.substr(0, prefix_len);
            children_t new_children = node->children;
            node->children          = {};
            node->children.push_back(
                {old_root_key.substr(prefix_len), node->value, new_children});
            // if new root key is same as key
            if (key.size() == prefix_len)
            {
                // then root is updated with the value
                node->value = value;
                return;
            }
            // otherwise set root to null and create new child;
            node->value = {};
        }
        // the only other possibility is that root key is the same as common
        // prefix

        auto child_key = std::string(key.substr(prefix_len));
        auto candidate = find_candidate(node->children, child_key);

        // if there is a candidate, update the candidate
        if (candidate.has_value())
        {
            update_node(candidate.value().second, child_key, value);
            return;
        }

        // there is no candidate, meaning we should create a new child
        node->children.push_back({child_key, value, {}});
    }

    std::string tabulate(int indent)
    {
        std::stringstream ss;
        for (int i = 0; i < indent; i++)
        {
            ss << " |";
        }
        return ss.str();
    };

    auto visualize_node(int indent, node* node) -> void
    {
        std::cout << tabulate(indent) << node->key << ":"
                  << (node->value.has_value()
                          ? std::to_string(node->value.value())
                          : "null")
                  << " [";
        for (auto& child : node->children)
        {
            std::cout << std::endl;
            visualize_node(indent + 1, &child);
        }
        if (node->children.size() > 0)
            std::cout << std::endl << tabulate(indent);

        std::cout << "]";
    };
};

int main()
{
    prefix_tree<int> t;
    t.set("abcdefg", 0);
    t.visualize_trie();
    t.set("abcdefo", 1);
    t.visualize_trie();
    t.set("abcdkop", 2);
    t.visualize_trie();
    t.set("abteflo", 3);
    t.visualize_trie();
    t.set("atefloo", 4);
    t.visualize_trie();
    t.remove("abcdefg");
    t.visualize_trie();
    t.set("aa", 5);
    t.remove("abcdefo");
    t.visualize_trie();
    t.remove("abcdkop");
    t.visualize_trie();
    t.set("abteflo", 20);
    t.visualize_trie();
    t.remove("atefloo");
    t.visualize_trie();
    t.remove("aa");
    t.visualize_trie();

    // comment to prevent collapse
}