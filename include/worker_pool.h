#ifndef WORKER_POOL_H
#define WORKER_POOL_H

#include <sema.h>

#include <atomic>
#include <cassert>
#include <condition_variable>
#include <functional>
#include <future>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>
#include <vector>

namespace fancy {

/**
 * Simplistic implementation of thread pool
 * using C++17.
 */
class worker_pool {
  public:
    /**
     * Creating worker pool with n workers.
     * By default the number of workers is equal to number
     * of cores on the machine.
     */
    worker_pool(long tcount = std::thread::hardware_concurrency(),
                bool _init = true)
        : worker_count(tcount), queue_sem(0), pool_state(is_running) {
        assert(tcount > 0);
        if (_init)
            init();
    }

    /**
     *  Initializing worker pool
     */
    void init() {
        for (long i = 0; i < worker_count; i++) {
            workers.emplace_back(std::thread(worker(this)));
        }
    }

    /**
     * Terminate all workers before getting destroyed
     */
    ~worker_pool() { stop(); }

    /**
     * No-copy and no-move
     */
    worker_pool(worker_pool const &) = delete;
    worker_pool &operator=(worker_pool const &) = delete;
    worker_pool(worker_pool &&) = delete;
    worker_pool &operator=(worker_pool &&) = delete;

    /**
     * Thread-safe job submition. Accepts any callable and
     * returns a future.
     *
     * @param f callable object
     *
     * @param Args optional arguments
     */
    template <typename F, typename... Args>
    auto submit(F f, Args ...args) -> std::future<decltype(f(args...))> {
        std::lock_guard l(queue_mutex);
        // Wrapping callable with arguments into a packaged task
        auto func = std::bind(std::forward<F>(f), std::forward<Args>(args)...);
        auto task_ptr =
            std::make_shared<std::packaged_task<decltype(f(args...))()>>(func);
        // Wrapping packaged task into a simple lambda for convenience
        job_queue.push([task_ptr] { (*task_ptr)(); });
        queue_sem.signal();
        return task_ptr->get_future();
    }

    /**
     * Terminate will stop all workers ignoring any remaining jobs.
     */
    void stop() {
        // do nothing if already terminated
        states temp = is_running;
        if (!pool_state.compare_exchange_strong(temp, is_stopping))
            return;
        // wakeup all workers
        queue_sem.signal(workers.size() + 1);
        // wait for each worker to terminate
        for (size_t i = 0; i < workers.size(); i++) {
            if (workers[i].joinable())
                workers[i].join();
        }
        pool_state = is_stopped;
    }

    /**
     * Check how many jobs remain in the queue
     */
    long jobs_remaining() {
        std::lock_guard l(queue_mutex);
        return job_queue.size();
    }

    /**
     * This function will block until all
     * the jobs in the queue have been processed
     */
    void wait_until_empty() {
        std::unique_lock l(queue_mutex);
        while (!(job_queue.empty() || pool_state != is_running))
            cv_empty.wait(l, [&] {
                return job_queue.empty() || pool_state != is_running;
            });
    }

    bool running() { return pool_state = is_running; }

    bool stopped() { return pool_state = is_stopped; }

    bool stopping() { return pool_state = is_stopping; }

  private:
    /**
     * Inner class that represents individual workers.
     */
    class worker {
      private:
        worker_pool *wp;

      public:
        worker(worker_pool *_wp) : wp(_wp){};

        /**
         * Main worker loop.
         */
        void operator()() {
            // work until asked to stop
            while (true) {
                auto t = wp->fetch();
                if (!t.has_value())
                    // when asked to stop workers will wake up
                    // and recieve a null
                    break;
                else
                    t.value()();
            }
        };
    };

    long worker_count;
    std::vector<std::thread> workers;
    std::queue<std::function<void(void)>> job_queue;
    // access control for the queue
    std::mutex queue_mutex;
    Semaphore queue_sem;

    // this is used to notify that queue has been emptied
    std::condition_variable cv_empty;

    // variable to communicate the worker pool state
    enum states { is_running, is_stopping, is_stopped };
    std::atomic<states> pool_state;

    /**
     * Thread safe job fetchind
     */
    std::optional<std::function<void(void)>> fetch() {
        queue_sem.wait();
        std::unique_lock l(queue_mutex);
        // return nothing if asked to stop
        if (pool_state != is_running)
            return std::nullopt;
        auto res = std::move(job_queue.front());
        // if we happen to have emptied the queue notify everyone who is waiting
        job_queue.pop();
        if (job_queue.empty())
            cv_empty.notify_all();
        return res;
    };
};
} // namespace fancy

#endif // WORKER_POOL_H