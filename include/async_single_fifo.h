#pragma once

#include <atomic>
#include <memory>
#include <optional>
#include <thread>
#include <vector>

namespace fancy {
/*
    [_][_][_][_] empty
     ^head_tail

    [x][x][_][_] partially filled
 head^     ^tail

    [x][x][_][x] full
       tail^  ^head

    1 cell is always empty to not to create ambiguity
    like tail=head meaning both full and empty
    
*/

using namespace std;

/**
 * Asynchronous non-blocking single consumer - single producer queue.
 * Implemented using atomics and circular buffer.
 */
template <typename T> class async_single_fifo {
  public:
    async_single_fifo(size_t n);

    // no copy no move
    async_single_fifo(async_single_fifo const &) = delete;
    async_single_fifo &operator=(async_single_fifo const &) = delete;
    async_single_fifo(async_single_fifo &&) = delete;
    async_single_fifo &operator=(async_single_fifo &&) = delete;

    bool inline empty();
    bool inline full();

    T front();
    optional<T> try_front();
    void pop();
    bool try_pop();
    bool try_push(T);
    void push(T);
    size_t size();

  protected:
    size_t next(size_t);
    atomic_size_t m_head;
    atomic_size_t m_tail;
    vector<T> m_data;
    size_t m_size;
};

/**
 * async_single_fifo constructor, initiates the buffer with n+1 cells.
 * One cell is added because it will remain empty due to queue design.
 *
 * @param n desired size of the buffer
 */
template <typename T> async_single_fifo<T>::async_single_fifo(size_t n) {
    m_head = 0;
    m_tail = 0;
    m_size = n + 1;
    m_data = vector<T>(m_size);
}

// NOTE - using this function works better (or at all) than inlining the code.
/**
 * Checks if the queue is empty.
 * The value may be out of date if the queue is in use.
 *
 * @return true if empty, false if not
 */
template <typename T> inline bool async_single_fifo<T>::empty() {
    return m_tail.load(memory_order_acquire) == m_head.load(memory_order_acquire);
}

// NOTE - see the note async_single_fifo::empty
/**
 * Checks if the queue is full.
 * The value may be out of date if the queue is in use.
 *
 * @return true if full, false if not
 */
template <typename T> inline bool async_single_fifo<T>::full() {
    return next(m_tail.load(memory_order_acquire)) == m_head.load(memory_order_acquire);
}

/**
 * Gets an item in the front of the queue.
 * This function willblock on empty until an item becomes available.
 *
 * @return the item in the front of the queue.
 */
template <typename T> T async_single_fifo<T>::front() {
    while (empty())
        this_thread::yield();
    auto res = m_data[m_head.load(memory_order_acquire)];
    return res;
}

/**
 * Gets an item in the front of the queue.
 * This function willimmediately return regardless of the state of the queue.
 *
 * @return optional containing the item or nothing
 */
template <typename T> optional<T> async_single_fifo<T>::try_front() {
    if (empty())
        return std::nullopt;
    auto e = m_data[m_head.load(memory_order_acquire)];
    return e;
}

/**
 * Pops an item in the font of the queue.
 * This function willimmediately return regardless of the state of the queue.
 *
 * @return true if successfully popped, false if not
 */
template <typename T> bool async_single_fifo<T>::try_pop() {
    while (empty())
        return false;
    auto head = m_head.load(std::memory_order_acquire);
    auto e = m_data[head];
    auto new_head = next(head);
    m_head.store(new_head, std::memory_order_release);
    return true;
}

/**
 * Pops an item in the front of the queue.
 * This function willblock until the item can be popped.
 */
template <typename T> void async_single_fifo<T>::pop() {
    while (empty())
        this_thread::yield();
    auto head = m_head.load(std::memory_order_acquire);
    auto e = m_data[head];
    auto new_head = next(head);
    m_head.store(new_head, memory_order_release);
}

/**
 * Pushes an item at the back of the queue.
 * This function willimmediately return regardless of the state of the queue.
 *
 * @return true if successfully inserted, false if not
 */
template <typename T> bool async_single_fifo<T>::try_push(T _arg) {
    if (full())
        return false;
    auto tail = m_tail.load(memory_order_acquire);
    auto new_tail = next(tail);
    m_data[tail] = _arg;
    m_tail.store(new_tail, memory_order_release);
    return true;
}

/**
 * Pushes an item at the back of the queue.
 * This function will block until the item is pushed.
 */
template <typename T> void async_single_fifo<T>::push(T _arg) {
    while (full())
        this_thread::yield();
    auto tail = m_tail.load(std::memory_order_acquire);
    auto new_tail = next(tail);
    m_data[tail] = _arg;
    m_tail.store(new_tail, memory_order_release);
}

/**
 * Gets the number of elements in the queue.
 * This value may be out of date if the queue is in use.
 *
 * @return the number of elements in the queue.
 */
template <typename T> size_t async_single_fifo<T>::size() {
    auto head = m_head.load(memory_order_acquire);
    auto tail = m_tail.load(memory_order_acquire);
    if (head > tail)
        tail += m_size;
    return tail - head;
}

/**
 * Calculates the next index in the circular buffer.
 *
 * @return next index
 */
template <typename T> inline size_t async_single_fifo<T>::next(size_t i) {
    size_t res = (i == m_size - 1) ? 0 : i + 1;
    return res;
}

} // namespace fancy